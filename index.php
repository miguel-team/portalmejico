<!DOCTYPE html>
<html>
    <head>
	<link rel="stylesheet" href="css/style.css" />
	<meta charset="utf-8">
  <script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
	<meta name="viewport" content="initial-scale = 1.0,maximum-scale = 1.0" />
  <script type="text/javascript" src="js/swfobject.js"></script>
  <script type="text/javascript" src="js/downloadify.min.js"></script>
    <title>XML Form</title>
    </head>
  <body onload="load();">
<div id="left-container">

    <div class="white" id="formcontainer">
    <h1 class="white">Portal Mexico XML</h1>
    <h2>Don't forget to check the output file!!!</h2>
    
    <form method="post" enctype="multipart/form-data" action="index.php">
      <table width="100%" border="0" cellspacing="0" cellpadding="5">
        <tr>
          <td width="15%"><label class="white" for="gol">GOL#:</label></td>
          <td width="70%"><input type="phone" class="validate[required,custom[onlyLetter]]" name="gol" id="gol" value="" /> 
          <?php $gol=$_POST['gol']; echo ($gol);?></td>          
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><label class="white" for="email">Video Name:</label></td>
          <td><input type="text" class="validate[required,custom[email]]" name="video" id="video" value="" /> 
          <?php $video=$_POST['video'];echo ($video);?></td>          
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td valign="top">&nbsp;</td>
          <td colspan="2">
          <input type="submit" value="Submit/Reset" />
          </td>
        </tr>
        <?php include 'file.php';?> 
      </table>
      </form>
      </div>
      </div>
      <div id="right-container">
      <div id="form-container">
      <form action="" method="post">
      <label for="filename">Filename: </label>
        <input type="text" name="filename" value=".xml" id="filename" edit="false"/>
        <textarea readonly cols="120" name="download" id="download" rows="34">
          <?php echo htmlspecialchars_decode($doc->saveXML());?>
        </textarea>
        <p id="downloadify">
        You must have Flash 10 installed to download this file.
      </p>
        <script type="text/javascript">
      function load(){
        Downloadify.create('downloadify',{
          filename: function(){
            return document.getElementById('filename').value;
          },
          data: function(){ 
            return document.getElementById('download').value;
          },
          swf: 'media/downloadify.swf',
          downloadImage: 'images/download.png',
          width: 100,
          height: 30,
          transparent: true,
          append: false
        });
      }
    </script>
      </form>      
      </div>
      </div>
  </body>
</html>

